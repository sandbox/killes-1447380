<?php

/**
 * @file
 * Provides form for changing settings
 */

/**
 * Implementation of the former hook_settings().
 */
function expire_admin_settings_form() {
  drupal_add_js(drupal_get_path('module', 'expire') .'/expire.admin.js');
  drupal_set_title(t('Cache Expiration'));

  $form['expire'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('What to expire'),
  );

  $form['expire']['expire_flush_nodes'] = array(
    '#type' => 'checkbox',
    '#title' => t('Expire node when modified'),
    '#default_value' => variable_get('expire_flush_nodes', EXPIRE_FLUSH_NODES),
    '#description' => t('When node is modified, flush the node.'),
  );
  $form['expire']['expire_node'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Node expiration options'),
    '#collapsible'   => TRUE,
    '#collapsed'     => FALSE,
    '#prefix'        => '<div id="expire-flush-nodes-options">',
    '#suffix'        => '</div>',
  );
  $form['expire']['expire_node']['expire_flush_front'] = array(
    '#type'        => 'checkbox',
    '#title'       => t('Expire front page'),
    '#default_value' => variable_get('expire_flush_front', EXPIRE_FLUSH_FRONT),
    '#description'   => t('When expiring a node: if promoted to front page, expire front page.'),
  );
  $form['expire']['expire_node']['expire_flush_node_terms'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Expire node term pages'),
    '#default_value' => variable_get('expire_flush_node_terms', EXPIRE_FLUSH_NODE_TERMS),
    '#description'   => t('When expiring a node: expire taxonomy pages for its terms.'),
  );

  $node_types = node_get_types();
  foreach ($node_types as $type => $obj) {
    $options[$type] = $obj->name;
  }
  natsort($options);

  $form['expire']['expire_node']['expire_nodes_excluded_node_types_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Node Types to Exclude'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['expire']['expire_node']['expire_nodes_excluded_node_types_fieldset']['expire_nodes_excluded_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Node types'),
    '#options' => $options,
    '#default_value' => variable_get('expire_nodes_excluded_node_types', array()),
    '#description' => t('Select any node types that you wish to exclude from expiration when a node is created, updated or deleted.'),
  );

  $form['expire']['expire_flush_comments'] = array(
    '#type' => 'checkbox',
    '#title' => t('Expire node when comment is modified'),
    '#default_value' => variable_get('expire_flush_comments', EXPIRE_FLUSH_COMMENTS),
    '#description' => t('When a comment is modified, flush the node attached to the comment.'),
  );
  $form['expire']['expire_comment'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Comment expiration options'),
    '#collapsible'   => TRUE,
    '#collapsed'     => FALSE,
    '#prefix'        => '<div id="expire-flush-comments-options">',
    '#suffix'        => '</div>',
  );
  $form['expire']['expire_comment']['expire_comments_excluded_node_types_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Node Types to Exclude'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['expire']['expire_comment']['expire_comments_excluded_node_types_fieldset']['expire_comments_excluded_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Node types'),
    '#options' => $options,
    '#default_value' => variable_get('expire_comments_excluded_node_types', array()),
    '#description' => t('Select any node types that you wish to exclude from expiration when a comment on a node is created, updated, deleted, published or unpublished.'),
  );

  $form['expire']['expire_flush_users'] = array(
    '#type' => 'checkbox',
    '#title' => t('Expire user pages'),
    '#default_value' => variable_get('expire_flush_users', EXPIRE_FLUSH_USERS),
    '#description' => t('When user is modified, flush the user page.'),
  );

  if (module_exists('votingapi')) {
    $form['expire']['expire_flush_voting'] = array(
      '#type' => 'checkbox',
      '#title' => t('Expire object via votingAPI events'),
      '#default_value' => variable_get('expire_flush_voting', EXPIRE_FLUSH_VOTING),
      '#description' => t('Flush objects (nodes, comments, users) when votingAPI events occur.'),
    );
    $form['expire']['expire_voting'] = array(
      '#type'          => 'fieldset',
      '#title'         => t('Voting expiration options'),
      '#collapsible'   => TRUE,
      '#collapsed'     => FALSE,
      '#prefix'        => '<div id="expire-flush-voting-options">',
      '#suffix'        => '</div>',
    );
    $form['expire']['expire_voting']['expire_voting_excluded_node_types_fieldset'] = array(
      '#type' => 'fieldset',
      '#title' => t('Node Types to Exclude'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['expire']['expire_voting']['expire_voting_excluded_node_types_fieldset']['expire_voting_excluded_node_types'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Node types'),
      '#options' => $options,
      '#default_value' => variable_get('expire_voting_excluded_node_types', array()),
      '#description' => t('Select any node types that you wish to exclude from expiration when a node is voted on using votingAPI.'),
    );
  }

  $form['expire']['expire_flush_menu_items'] = array(
    '#type'          => 'radios',
    '#title'         => t('Expire menus'),
    '#options'       => array(0 => t('No'), EXPIRE_FLUSH_MENU_ITEMS => t('Family'), 2 => t('Entire menu')),
    '#default_value' => variable_get('expire_flush_menu_items', EXPIRE_FLUSH_MENU_ITEMS),
    '#description'   => t('When expiring a node: expire related menu items or entire menu'),
  );
  $form['expire']['expire_flush_cck_references'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Expire CCK node references'),
    '#default_value' => variable_get('expire_flush_cck_references', EXPIRE_FLUSH_CCK_REFERENCES),
    '#description'   => t('When expiring a node: expire its node references and nodes containing it in their own node references.'),
  );

  $form['format'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Expire protocol'),
  );
  $form['format']['expire_include_base_url'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Include base URL in expires'),
    '#default_value' => variable_get('expire_include_base_url', EXPIRE_INCLUDE_BASE_URL),
    '#description'   => t('Include the base URL in expire requests. Compatible with Domain Access'),
  );

  return system_settings_form($form);
}
