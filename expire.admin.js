Drupal.behaviors.expire = function(context) {
  $('#edit-expire-flush-nodes:not(.edit-expire-flush-nodes-processed)', context).addClass('edit-expire-flush-nodes-processed').each(function() {
    // Default state
    if ($(this).is(':checked')) {
      $('#expire-flush-nodes-options').show();
    }
    else {
      $('#expire-flush-nodes-options').hide();
    }

    // When checkbox is checked
    $(this).change(function() {
      $('#expire-flush-nodes-options').toggle();
    });
  });

  $('#edit-expire-flush-comments:not(.edit-expire-flush-comments-processed)', context).addClass('edit-expire-flush-comments-processed').each(function() {
    // Default state
    if ($(this).is(':checked')) {
      $('#expire-flush-comments-options').show();
    }
    else {
      $('#expire-flush-comments-options').hide();
    }

    // When checkbox is checked
    $(this).change(function() {
      $('#expire-flush-comments-options').toggle();
    });
  });

  $('#edit-expire-flush-voting:not(.edit-expire-flush-voting-processed)', context).addClass('edit-expire-flush-voting-processed').each(function() {
    // Default state
    if ($(this).is(':checked')) {
      $('#expire-flush-voting-options').show();
    }
    else {
      $('#expire-flush-voting-options').hide();
    }

    // When checkbox is checked
    $(this).change(function() {
      $('#expire-flush-voting-options').toggle();
    });
  });
};